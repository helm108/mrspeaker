ALTER TABLE mrspeaker.big5 ALTER COLUMN deadlift TYPE real USING deadlift::real;
ALTER TABLE mrspeaker.big5 ALTER COLUMN bench_press TYPE real USING bench_press::real;
ALTER TABLE mrspeaker.big5 ALTER COLUMN squat TYPE real USING squat::real;
ALTER TABLE mrspeaker.big5 ALTER COLUMN shoulder_press TYPE real USING shoulder_press::real;
ALTER TABLE mrspeaker.big5 ALTER COLUMN barbell_row TYPE real USING barbell_row::real;
