CREATE TABLE mrspeaker.users
(
    id          SERIAL PRIMARY KEY,
    telegram_id NUMERIC UNIQUE,
    username    VARCHAR,
    updated_on  TIMESTAMP default CURRENT_TIMESTAMP
);

CREATE INDEX users_telegram_id ON mrspeaker.users (telegram_id);

CREATE TRIGGER update_user_task_updated_on
    BEFORE UPDATE
    ON
        mrspeaker.users
    FOR EACH ROW
EXECUTE PROCEDURE mrspeaker.update_updated_on_user_task();
