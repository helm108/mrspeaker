CREATE TABLE mrspeaker.big5
(
    id             SERIAL PRIMARY KEY,
    telegram_id    NUMERIC,
    deadlift       INTEGER,
    bench_press    INTEGER,
    squat          INTEGER,
    shoulder_press INTEGER,
    barbell_row   INTEGER
);

CREATE INDEX big5_telegram_id ON mrspeaker.big5 (telegram_id);
