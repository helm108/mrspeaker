CREATE TABLE mrspeaker.weather
(
    id          SERIAL PRIMARY KEY,
    telegram_id NUMERIC UNIQUE,
    name        VARCHAR,
    state       VARCHAR,
    country     VARCHAR,
    lat         VARCHAR,
    lon         VARCHAR
);

CREATE INDEX weather_telegram_id ON mrspeaker.weather (telegram_id);
