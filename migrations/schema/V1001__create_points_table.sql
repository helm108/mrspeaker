CREATE TABLE mrspeaker.points
(
    id          SERIAL PRIMARY KEY,
    telegram_id NUMERIC,
    chat_id     NUMERIC,
    points      NUMERIC
);

CREATE INDEX points_telegram_id ON mrspeaker.points (telegram_id);
