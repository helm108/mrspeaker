CREATE TABLE mrspeaker.chats
(
    id          SERIAL PRIMARY KEY,
    telegram_id NUMERIC,
    chat_id      NUMERIC
);

CREATE INDEX chats_telegram_id ON mrspeaker.chats (telegram_id);
