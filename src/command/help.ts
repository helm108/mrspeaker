import CommandResponse from '../model/CommandResponse.js';
import CommandClass from '../model/CommandClass.js';

export default class Help implements CommandClass {

    public name: string = 'help';
    public description: string = 'Help command.';

    async handle(): Promise<CommandResponse> {
        return {
            message: 'Please visit https://mr-speaker-prod.fly.dev/ for more information.',
        }
    }
}
