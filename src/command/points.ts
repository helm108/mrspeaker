import CommandResponse from '../model/CommandResponse.js';
import CommandClass from '../model/CommandClass.js';
import PointsService from '../service/PointsService.js';
import UserService from '../service/UserService.js';
import User from '../model/User.js';
import OutputService from '../service/OutputService.js';

interface PointsCommand {
    username: string;
    value: number;
}

/**
 * Timezone Codes: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
 */
export default class Points implements CommandClass {
    private userService: UserService;
    private pointsService: PointsService;
    private outputService: OutputService;

    public name: string = 'points';
    public description: string = 'Award or remove points from people.';

    constructor(userService: UserService, pointsService: PointsService, outputService: OutputService) {
        this.userService = userService;
        this.pointsService = pointsService;
        this.outputService = outputService;
    }

    private parseMessage(message: string): PointsCommand {
        const messageParts = message.split(' ');

        if (messageParts.length > 2) {
            throw new Error('Too many parts to this message');
        }

        const username = messageParts.find(part => part.startsWith('@'))?.replace('@', '');
        const originalValue = messageParts.find(part => !part.startsWith('@'));
        const value = parseInt(originalValue);

        if (!username) {
            throw new Error('You must specify a username.');
        }

        if (isNaN(value)) {
            throw new Error(`'${originalValue}' is not a number.`);
        }

        return {username, value};
    }

    private async listPoints(chatId: number): Promise<CommandResponse> {
        const points = await this.pointsService.getPointsForChatRoom(chatId);
        if (points === null ) {
            return {
                message: 'Nobody has any points in this chat.',
            }
        }

        points.sort((a, b) => {
            return a.points < b.points ? 1 : a.points > b.points ? -1 : 0;
        });

        const tableData: string[][] = [];
        points.forEach(point => {
            tableData.push([point.username, point.points.toString()]);
        });

        return {
            message: this.outputService.toTable(tableData),
            options: {
                parse_mode: 'MarkdownV2',
            },
        }
    }

    private async addPoints(text: string, chatId: number, senderUsername: string): Promise<CommandResponse> {
        const { username, value } = this.parseMessage(text);

        if (username === senderUsername) {
            throw new Error('You cannot award yourself points');
        }

        const user: User = await this.userService.getUserByUsername(username);

        const newTotal = await this.pointsService.addPoints(user.telegramId, chatId, value);

        const response = value > 0 ?
            `Added ${value} points to @${username}. New total: ${newTotal}` :
            `Subtracted ${Math.abs(value)} points from @${username}. New total: ${newTotal}`;

        return { message: response };
    }

    async handle(ctx, cleanText): Promise<CommandResponse> {
        const chatId = ctx.update.message.chat.id;
        const senderUsername = ctx.update.message.from.username;

        return cleanText === '' ?
            await this.listPoints(chatId) :
            await this.addPoints(cleanText, chatId, senderUsername);
    }
}
