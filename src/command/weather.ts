import CommandResponse from '../model/CommandResponse.js';
import CommandClass from '../model/CommandClass.js';
import WeatherService from "../service/WeatherService.js";

export default class Weather implements CommandClass {
    public name: string = 'weather';
    public description: string = 'Show the weather for a given location.';

    private helpText: string = `Run \`/weather <location>\` to get the weather for a location.
Run \`/weather set <location>\` to save a location.
Run \`/weather\` to get the weather for your saved location.
Be setting when saving your location: "city, state, country" works best, e.g. London, England, UK or London, Ontario, CA.`;

    private weatherService: WeatherService;

    constructor(weatherService: WeatherService) {
        this.weatherService = weatherService;
    }

    async handle(ctx, cleanText): Promise<CommandResponse> {
        const userId = ctx.update.message.from.id;

        if (cleanText === '') {
            try {
                return await this.weatherService.getWeather(null, userId);
            }
            catch (e) {
                return {
                    message: `${e.message} - Unable to get weather.`,
                }
            }
        } else if (cleanText.startsWith('help')) {
            return {
                message: this.helpText,
                options: {
                    parse_mode: 'MarkdownV2',
                },
            };
        } else if (cleanText.startsWith('set')) {
            try {
                const location = cleanText.replace('set', '').trim();
                return await this.weatherService.setLocation(location, userId);
            }
            catch (e) {
                return {
                    message: `${e.message}. Unable to set location.`,
                }
            }
        } else {
            try {
                const response = await this.weatherService.getWeather(cleanText, userId);
                return response === null ? {message: this.helpText} : response;
            }
            catch (e) {
                return {
                    message: `${e.message} - Unable to get weather.`,
                }
            }
        }
    }
}
