import {DateTime} from 'luxon';
import CommandResponse from '../model/CommandResponse.js';
import CommandClass from '../model/CommandClass.js';
import OutputService from '../service/OutputService.js';

interface DisplayTime {
    name: string;
    timezone: string;
    time?: string;
    sortTime?: string;
}

/**
 * Timezone Codes: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
 */
export default class Times implements CommandClass {
    public name: string = 'times';
    public description: string = 'Displays the current time in a variety of timezones.';

    private outputService: OutputService;

    constructor(outputService: OutputService) {
        this.outputService = outputService;
    }

    private displayTimes: DisplayTime[] = [
        {
            name: 'New York',
            timezone: 'America/New_York',
        },
        {
            name: 'UTC',
            timezone: 'UTC',
        },
        {
            name: 'London',
            timezone: 'Europe/London',
        },
        {
            name: 'Amsterdam',
            timezone: 'Europe/Amsterdam',
        },
        {
            name: 'Tokyo',
            timezone: 'Asia/Tokyo',
        },
        {
            name: 'Melbourne',
            timezone: 'Australia/Melbourne',
        },
    ];

    private generateTable(): string {
        const tableData: string[][] = [];
        this.displayTimes.forEach(displayTime => {
            tableData.push([displayTime.name, displayTime.time]);
        });

        return this.outputService.toTable(tableData);
    }

    async handle(ctx): Promise<CommandResponse> {
        const dateTime = DateTime.fromSeconds(ctx.message.date);

        this.displayTimes.forEach(displayTime => {
                const timeZone = dateTime.setZone(displayTime.timezone);
                displayTime.time = timeZone.toFormat('EEE dd, HH:mm');
                displayTime.sortTime = timeZone.toISO();
            }
        );

        this.displayTimes.sort((a, b) => {
            return a.sortTime > b.sortTime ? 1 : a.sortTime < b.sortTime ? -1 : 0;
        });

        return {
            message: this.generateTable(),
            options: {
                parse_mode: 'MarkdownV2',
            },
        };
    }
}
