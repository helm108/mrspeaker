import CommandResponse from '../model/CommandResponse.js';
import CommandClass from '../model/CommandClass.js';
import Big5Service from '../service/big5/Big5Service.js';

export default class Big5 implements CommandClass {
    private big5Service: Big5Service;

    public name: string = 'big5';
    public description: string = 'Track Big 5 weights.';

    private helpText: string = `\`/big5\` to see weights for chat.
\`/big5 @username\` to see weights for user.
\`/big5 <exercise> <weight>\` to set your weight for an exercise.
E.g. \`/big5 bench 100\` to set your bench press to 100kg.
You can set multiple with \`/big5 ben 100 sq 50\``;

    constructor(big5Service: Big5Service) {
        this.big5Service = big5Service;
    }

    async handle(ctx, cleanText): Promise<CommandResponse> {
        const userId = ctx.update.message.from.id;
        const chatId = ctx.update.message.chat.id;

        if (cleanText === '') {
            const response = await this.big5Service.getChannelDetails(chatId);
            response.options.reply_to_message_id = ctx.update.message.message_id;
            return response;
        } else if (cleanText.startsWith('help')) {
            return {
                message: this.helpText,
                options: {
                    parse_mode: 'MarkdownV2',
                },
            };
        } else if (cleanText.startsWith('@')) {
            const response = await this.big5Service.getUserDetails(cleanText.replace('@', ''));
            response.options.reply_to_message_id = ctx.update.message.message_id;
            return response;
        }
        else {
            const response = await this.big5Service.setValues(userId, chatId, cleanText);
            response.options.reply_to_message_id = ctx.update.message.message_id;
            return response;
        }
    }
}
