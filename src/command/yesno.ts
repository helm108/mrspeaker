import CommandResponse from '../model/CommandResponse.js';
import CommandClass from '../model/CommandClass.js';
import SeededRandom from '../service/SeededRandom.js';

export default class YesNo implements CommandClass {
    public name: string = 'mrspeaker';
    public description: string = 'Ask a yes/no question and get an answer.';

    private seededRandom: SeededRandom;

    constructor(seededRandom: SeededRandom) {
        this.seededRandom = seededRandom;
    }

    private async respond(text: string): Promise<string> {
        if (!text.endsWith('?')) {
            return 'That is not a question.';
        }

        const value = this.seededRandom.random(text);

        return value < 0.45 ? 'Yes.' : value < 0.9 ? 'No.' : `Maybe.`;
    }

    async handle(ctx, cleanText): Promise<CommandResponse> {
        return {
            message: await this.respond(cleanText),
        }
    }
}
