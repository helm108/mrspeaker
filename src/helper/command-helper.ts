export default class CommandHelper {
    /**
     * Removes the first word from a string.
     * Turns `/command value other ` into `value other`.
     *
     * @param command
     */
    public stripCommand(command: string): string {
        const messageParts = command.trim().split(' ');
        messageParts.shift();
        return messageParts.join(' ').trim();
    }
}
