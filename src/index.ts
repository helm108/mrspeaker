import 'dotenv/config';
import MTProto from '@mtproto/core';
import Server from './server.js';
import BotService from './service/BotService.js';
import {setCommands} from './commandProcessor.js';
import EnvVarService from './service/EnvVarService.js';
import MTProtoService from './service/MTProtoService.js';
import path from 'path';
import {Telegraf} from 'telegraf';

const env = new EnvVarService();

const telegraf = new Telegraf(env.telegramBotToken);
const serverService = new Server(env);
const botService = new BotService(env, telegraf);

const mtproto = new MTProto({
    api_id: env.telegramApiId,
    api_hash: env.telegramApiHash,

    storageOptions: {
        path: path.resolve(path.resolve(), 'data/1.json')
    }
});

const telegramApiService = new MTProtoService(env, mtproto);

async function init() {
    // await telegramApiService.botLogin();
    const bot = await botService.createBot();

    const commands = await setCommands(bot, telegramApiService);

    const server = await serverService.createServer(bot, botService.secretPathComponent, commands);

    server.listen(env.port, () => console.log(`App listening on port ${env.port}`));

    await bot.telegram.sendMessage(env.ownerTelegramId, 'Deployed');
}

init().catch(reason => console.log(reason));
