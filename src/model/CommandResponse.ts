export default interface CommandResponse {
    message: string;
    options?: any;
}
