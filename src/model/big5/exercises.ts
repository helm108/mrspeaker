export default interface Exercises {
    deadlift?: number;
    bench_press?: number;
    squat?: number;
    shoulder_press?: number;
    barbell_row?: number;
}
