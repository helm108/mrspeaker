export default interface UserExercise {
    id: number;
    telegram_id: number;
    deadlift: number;
    bench_press: number;
    squat: number;
    shoulder_press: number;
    barbell_row: number;
}
