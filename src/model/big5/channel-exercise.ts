export default interface ChannelExercise {
    username: string;
    total: number;
}
