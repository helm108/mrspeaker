import {Context} from 'telegraf';
import {MountMap} from 'telegraf/typings/telegram-types';
import CommandResponse from './CommandResponse.js';

export default interface CommandClass {
    name: string;
    description: string;
    handle(ctx: Context<MountMap['text']>, cleanText: string): Promise<CommandResponse>;
}
