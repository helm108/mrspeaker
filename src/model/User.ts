export default interface User {
    telegramId: number;
    username: string;
    updatedOn?: string;
}
