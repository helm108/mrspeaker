export default interface PointAssignment {
    username: string;
    points: number;
}
