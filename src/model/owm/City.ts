export default interface City {
    name: string;
    state: string;
    country: string;
    lat: string;
    lon: string;
}
