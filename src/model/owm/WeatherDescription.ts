export default interface WeatherDescription {
    id: number;
    main: string;
    description: string;
    icon: string;
}
