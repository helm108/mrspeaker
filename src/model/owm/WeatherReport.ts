import Daily from './Daily.js';
import Current from "./Current.js";
import Alert from "./Alert.js";

export default interface WeatherReport {
    timezone: string;
    timezone_offset: string;
    current: Current;
    daily: Daily[],
    alerts: Alert[];
}
