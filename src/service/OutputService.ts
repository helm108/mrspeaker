export default class OutputService {

    public escapeMarkdownV2(input: string): string {
        const regex = /[\(\)\.\[\]\-\*\_~>=|{}!]/gm;
        return input.replace(regex, '\\$&');
    }

    public toTable(input: string[][]) {
        let output = '';
        let colCount = input[0].length;

        const spacer = 1;

        for (let colIndex = 0; colIndex < colCount; colIndex++) {
            let colWidth = 0;

            for (let rowIndex = 0; rowIndex < input.length; rowIndex++) {
                const cell = input[rowIndex][colIndex];
                colWidth = cell.length > colWidth ? cell.length : colWidth;
            }

            for (let rowIndex = 0; rowIndex < input.length; rowIndex++) {
                const isLastCol: boolean = colIndex === colCount - 1;
                input[rowIndex][colIndex] = input[rowIndex][colIndex].padEnd(isLastCol ? colWidth : colWidth + spacer)
            }
        }

        for(let rowIndex = 0; rowIndex < input.length; rowIndex++) {
            const row = input[rowIndex];
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                output += row[colIndex];
            }
            output += rowIndex < input.length - 1 ? '\n' : '';
        }

        return '```\n' + output + '\n```';
    }
}
