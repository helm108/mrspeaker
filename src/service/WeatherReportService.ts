import {DateTime} from 'luxon';

import WeatherReport from '../model/owm/WeatherReport.js';
import City from '../model/owm/City.js';
import OutputService from "./OutputService.js";

export default class WeatherReportService {
    private outputService: OutputService;

    constructor(outputService: OutputService) {
        this.outputService = outputService;
    }

    private temp(temperature: number): string {
        return `${(Math.round(temperature)).toString()}°C`;
    }

    private uvi(uvi: number): string {
        let severity: string;
        if (uvi < 3) severity = 'Low';
        if (uvi >= 3 && uvi < 6) severity = 'Moderate';
        if (uvi >= 6 && uvi < 8) severity = 'High';
        if (uvi >= 8 && uvi < 11) severity = 'Very High';
        if (uvi > 11) severity = 'Extreme';

        return `${Math.round(uvi)} (${severity})`;
    }

    private currentReport(weatherReport: WeatherReport): string {
        const current = weatherReport.current;
        const currentDay = weatherReport.daily[0];

        const sunrise: string = DateTime.fromSeconds(current.sunrise).setZone(weatherReport.timezone).toFormat('HH:mm');
        const sunset: string = DateTime.fromSeconds(current.sunset).setZone(weatherReport.timezone).toFormat('HH:mm');

        let table = [
            ['Temp', `${this.temp(current.temp)} (${this.temp(currentDay.temp.min)} to ${this.temp(currentDay.temp.max)})`],
            ['Feels Like', this.temp(current.feels_like)],
            ['Sunlight', `${sunrise} to ${sunset}`],
        ];
        current.weather.forEach((item) => {
            let weather;
            if (item.description && item.description.length > 1) {
                weather = item.description.charAt(0).toUpperCase() + item.description.slice(1);
            }
            else {
                weather = item.main;
            }

            table.push(['Weather', weather]);
        });

        table = table.concat([
            ['UV Index', this.uvi(current.uvi)],
            ['Max UVI', this.uvi(currentDay.uvi)],
            ['Rain Chance', `${currentDay.pop * 100}%`],
            ['Cloud Cover', `${currentDay.clouds}%`],
        ]);

        return this.outputService.toTable(table);
    }

    private tomorrowReport(weatherReport: WeatherReport): string {
        const tomorrow = weatherReport.daily[1];

        const table = [
            ['Temp', `${this.temp(tomorrow.temp.min)} to ${this.temp(tomorrow.temp.max)}`],
            ['Max UVI', this.uvi(tomorrow.uvi)],
            ['Rain Chance', `${tomorrow.pop * 100}%`],
            ['Cloud Cover', `${tomorrow.clouds}%`],
        ];

        tomorrow.weather.forEach((item) => {
            let weather;
            if (item.description && item.description.length > 1) {
                weather = item.description.charAt(0).toUpperCase() + item.description.slice(1);
            }
            else {
                weather = item.main;
            }

            table.push(['Weather', weather]);
        });

        return this.outputService.toTable(table);
    }

    private alertReport(weatherReport: WeatherReport): string {
        const table = [];

        weatherReport.alerts.forEach(alert => {
            const startTime = DateTime.fromSeconds(alert.start).setZone(weatherReport.timezone).toFormat('yyyy-MM-dd HH:mm');
            const endTime = DateTime.fromSeconds(alert.start).setZone(weatherReport.timezone).toFormat('yyyy-MM-dd HH:mm');
            table.push(['Source', alert.sender_name]);
            table.push(['From', startTime]);
            table.push(['To', endTime]);
            table.push(['Event', alert.event]);
            table.push(['Description', alert.description]);
        })

        return this.outputService.toTable(table);
    }

    public buildReport(city: City, weatherReport: WeatherReport): string {
        let location: string = `The weather for ${city.name},`;
        location += city.state ? ` ${city.state},` : '';
        location += ` ${city.country}:`;
        let response: string = location;

        response += '\n' + this.currentReport(weatherReport);
        response += '\nTomorrow:\n' + this.tomorrowReport(weatherReport);

        if (weatherReport.alerts && weatherReport.alerts.length > 0) {
            response += '\nAlerts:\n' + this.alertReport(weatherReport);
        }

        return response;
    }
}
