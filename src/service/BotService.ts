import {Telegraf} from 'telegraf';
import EnvVarService from './EnvVarService.js';

export default class BotService {
    private env: EnvVarService;
    private readonly telegraf: Telegraf;

    public secretPathComponent: string = '';

    constructor(envVarService: EnvVarService, telegraf: Telegraf) {
        this.env = envVarService;
        this.telegraf = telegraf;
    }

    async createBot(): Promise<Telegraf> {
        this.secretPathComponent = this.telegraf.secretPathComponent();

        if (this.env.isProduction) {
            console.log('==== Setting Webhook');
        } else {
            console.log('==== Polling');
            this.telegraf.launch().catch(reason => console.error(reason));
        }

        process.once('SIGINT', () => this.telegraf.stop('SIGINT'));
        process.once('SIGTERM', () => this.telegraf.stop('SIGTERM'));

        return this.telegraf;
    }
}
