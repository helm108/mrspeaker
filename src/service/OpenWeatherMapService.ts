import City from '../model/owm/City.js';
import WeatherReport from '../model/owm/WeatherReport.js';
import EnvVarService from './EnvVarService.js';

export default class OpenWeatherMapService {
    private geoUrl: string = 'https://api.openweathermap.org/geo/1.0/direct';
    private weatherUrl: string = 'https://api.openweathermap.org/data/3.0/onecall';

    private envVarService: EnvVarService;

    constructor(envVarService: EnvVarService) {
        this.envVarService = envVarService;
    }

    public async geoLookup(location: string): Promise<City> {
        const params: URLSearchParams = new URLSearchParams();
        params.set('appid', this.envVarService.openWeatherMapApikey);
        params.set('limit', '1');
        params.set('q', location);

        const url = `${this.geoUrl}?${params.toString()}`;

        const cities: City[] = await (await fetch(url)).json();
        return cities.length > 0 ? cities[0] : null;
    }

    public async getWeatherReport(lat: string, lon: string): Promise<WeatherReport> {
        const params: URLSearchParams = new URLSearchParams();
        params.set('appid', this.envVarService.openWeatherMapApikey);
        params.set('exclude', 'minutely,hourly');
        params.set('units', 'metric');
        params.set('lat', lat);
        params.set('lon', lon);

        const url = `${this.weatherUrl}?${params.toString()}`;

        const response = await (await fetch(url)).json();

        return response === '[]' ? null : response;
    }
}
