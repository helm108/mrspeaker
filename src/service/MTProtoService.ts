import MTProto from '@mtproto/core';
import EnvVarService from './EnvVarService.js';
import {sleep} from '@mtproto/core/src/utils/common/index.js';
import User from '../model/User.js';

export default class MTProtoService {
    private env: EnvVarService;
    public mtproto: MTProto;

    constructor(envVarService: EnvVarService, mtproto: MTProto) {
        this.env = envVarService;
        this.mtproto = mtproto;
    }

    private async call(method, params = {}, options = {}) {

        if (!Object.keys(options).includes('dcId')) {
            Object.assign(options, {dcId: 4});
        }

        try {
            console.log(`Calling ${method}.`);
            const response = await this.mtproto.call(method, params, options);
            console.log(`${method} call successful.`);
            return response;
        } catch (error) {
            console.log(`${method} error:`, error);
            const {error_code, error_message} = error;

            if (error_code === 420) {
                const seconds = Number(error_message.split('FLOOD_WAIT_')[1]);
                await sleep(seconds * 1000);
                return this.call(method, params, options);
            }

            if (error_code === 303) {
                const [type, dcIdAsString] = error_message.split('_MIGRATE_');

                const dcId = Number(dcIdAsString);

                // If auth.sendCode call on incorrect DC need change default DC, because
                // call auth.signIn on incorrect DC return PHONE_CODE_EXPIRED error
                if (type === 'PHONE') {
                    await this.mtproto.setDefaultDc(dcId);
                } else {
                    Object.assign(options, {dcId});
                }

                return this.call(method, params, options);
            }

            return Promise.reject(error);
        }
    }

    async botLogin() {
        return this.call('auth.importBotAuthorization', {
                api_id: this.env.telegramApiId,
                api_hash: this.env.telegramApiHash,
                bot_auth_token: this.env.telegramBotToken,
            }
        );
    }

    async getUser(username: string): Promise<User> {
        let response;
        try {
            response = await this.call('contacts.resolveUsername', {username})
                .catch(() => {
                    throw new Error('Could not find a user with that name.');
                });
        } catch (e) {
            throw new Error(e.message);
        }

        if (response.users.length === 0) {
            throw new Error('Could not find a user with that name.');
        }

        return {
            telegramId: response.users[0].id,
            username,
        };
    }
}
