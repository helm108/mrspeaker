import MTProtoService from './MTProtoService.js';
import User from '../model/User.js';
import DatabaseService from './DatabaseService.js';

export default class UserService {
    private api: MTProtoService;
    private databaseService: DatabaseService;

    constructor(api: MTProtoService, databaseService: DatabaseService) {
        this.api = api;
        this.databaseService = databaseService;
    }

    private async getUserFromDatabase(username: string): Promise<User | null> {
        const query = {
            name: 'get-user',
            text: 'SELECT * FROM mrspeaker.users WHERE username = $1',
            values: [username],
        }

        const response = await this.databaseService.query(query);

        if (response.rowCount === 0) {
            return null;
        }

        return {
            telegramId: response.rows[0].telegram_id,
            username,
            updatedOn: response.rows[0].updated_on,
        }
    }

    public async addUser(userId: number, username: string) {
        const query = {
            name: 'upsert-user',
            text: `INSERT INTO mrspeaker.users (telegram_id, username)
                   VALUES ($1, $2)
                   ON CONFLICT (telegram_id)
                   DO UPDATE SET username = $2`,
            values: [userId, username],
        };

        await this.databaseService.query(query);
    }

    public async addUserChatAssociation(userId: number, chatId: number) {
        const getAssocation = {
            name: 'get-association',
            text: 'SELECT * FROM mrspeaker.chats WHERE telegram_id = $1 AND chat_id = $2',
            values: [userId, chatId],
        }

        const response = await this.databaseService.query(getAssocation);

        if (response.rowCount === 0) {
            const setAssociation = {
                name: 'set-association',
                text: 'INSERT INTO mrspeaker.chats (telegram_id, chat_id) VALUES ($1, $2)',
                values: [userId, chatId],
            }

            await this.databaseService.query(setAssociation);
        }
    }

    public async getUserByUsername(username: string): Promise<User> {
        // Get user from database.
        let user = await this.getUserFromDatabase(username);

        if (user === null) {
            user = await this.api.getUser(username);
            await this.addUser(user.telegramId, username);
        }

        // If updated_on is old, refresh.

        return user;
    }
}
