import CommandResponse from '../../model/CommandResponse.js';
import Big5InputService from './Big5InputService.js';
import Exercises from '../../model/big5/exercises.js';
import Big5QueryService from './Big5QueryService.js';
import UserService from '../UserService.js';
import User from '../../model/User.js';
import OutputService from '../OutputService.js';
import ChannelExercise from '../../model/big5/channel-exercise.js';

export default class Big5Service {
    private inputService: Big5InputService;
    private queryService: Big5QueryService;
    private userService: UserService;
    private outputService: OutputService;

    constructor(inputService: Big5InputService, queryService: Big5QueryService, userService: UserService, outputService: OutputService) {
        this.inputService = inputService;
        this.queryService = queryService;
        this.userService = userService;
        this.outputService = outputService;
    }

    private formatUserExercises(exercises: Exercises): string {
        const { squat, deadlift, bench_press, shoulder_press, barbell_row } = exercises;

        const total: number = squat + deadlift + bench_press + shoulder_press + barbell_row;

        const rows = [
            ['Squat', `${squat}kg`],
            ['Deadlift', `${deadlift}kg`],
            ['Bench Press', `${bench_press}kg`],
            ['Shoulder Press', `${shoulder_press}kg`],
            ['Barbell Rows', `${barbell_row}kg`],
            ['Total', `${total}kg`],
        ];

        return this.outputService.toTable(rows);
    }

    private formatChannelExercises(channelExercises: ChannelExercise[]): string {
        const rows = channelExercises.sort((a, b) => {
            return b.total - a.total;
        }).map((exercise: ChannelExercise) => {
            const { username, total } = exercise;
            return [username, `${total}kg`];
        });

        rows[0][1] += ' 👑';

        return this.outputService.toTable(rows);
    }

    public async getChannelDetails(chatId: string): Promise<CommandResponse> {
        // get all users in channel
        const exercises = await this.queryService.getChannelUsers(chatId);

        if (!exercises || exercises.length === 0) {
            return {
                message: 'No users have set their exercises.',
            }
        }

        return {
            message: this.formatChannelExercises(exercises),
            options: {
                parse_mode: 'MarkdownV2',
            },
        }
    }

    public async getUserDetails(username: string): Promise<CommandResponse> {
        const user: User = await this.userService.getUserByUsername(username);

        if (!user) {
            return {
                message: 'User does not exist.',
            }
        }

        const userExercises = await this.queryService.getUser(user.telegramId);

        if (!userExercises) {
            return {
                message: 'User does not have any exercises set.',
            }
        }

        return {
            message: `Weights for ${username}:\n${this.formatUserExercises(userExercises)}`,
            options: {
                parse_mode: 'MarkdownV2',
            },
        }
    }

    private getTotal(exercises: Exercises): number {
        return exercises.squat + exercises.deadlift + exercises.bench_press + exercises.shoulder_press + exercises.barbell_row;
    }

    public async setValues(userId: number, chatId: number, values: string): Promise<CommandResponse> {
        const existingUserExercises = await this.queryService.getUser(userId);

        const exercises: Exercises = this.inputService.handle(values);
        const isSet = await this.queryService.setExercises(userId, chatId, exercises);

        if (!isSet) {
            return {
                message: 'Failed to set exercises.',
            }
        }

        const userExercises = await this.queryService.getUser(userId);

        let diffMessage = null;

        if (existingUserExercises) {
            const total = this.getTotal(userExercises);
            const existingTotal = this.getTotal(existingUserExercises);

            if (total > existingTotal) {
                diffMessage = `Total increased by ${total - existingTotal}kg from ${existingTotal}kg to ${total}kg`;
            }
        }

        return {
            message: `Weights updated:\n\n${this.formatUserExercises(userExercises)}${diffMessage ? `\n${diffMessage}` : ''}`,
            options: {
                parse_mode: 'MarkdownV2',
            },
        }
    }
}
