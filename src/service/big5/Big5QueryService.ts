import DatabaseService from '../DatabaseService.js';
import Exercises from '../../model/big5/exercises.js';
import CommandResponse from '../../model/CommandResponse.js';
import OutputService from '../OutputService.js';
import UserExercise from '../../model/big5/user-exercise.js';
import ChannelExercise from '../../model/big5/channel-exercise.js';

export default class Big5QueryService {
    private databaseService: DatabaseService;

    constructor(databaseService: DatabaseService) {
        this.databaseService = databaseService;
    }

    private async getUserDetails(userId: number): Promise<UserExercise> {
        const userDetails = await this.databaseService.query({
            name: 'get-user-big5',
            text: 'SELECT * FROM mrspeaker.big5 WHERE telegram_id = $1',
            values: [userId],
        });

        if (userDetails.rowCount === 0) {
            return null;
        }

        return userDetails.rows[0];
    }

    private async setUserWeights(userId: number, exercises: Exercises): Promise<boolean> {
        const values: (string | number)[] = [userId];

        const setValues = [];
        Object.keys(exercises).forEach(key => {
            values.push(exercises[key]);
            setValues.push(`${key} = $${values.length}`);
        });

        if (setValues.length === 0) {
            throw new Error('No exercises set.');
        }

        const response = await this.databaseService.query({
            name: 'set-big5',
            text: `UPDATE mrspeaker.big5
                   SET ${setValues.join(', ')}
                   WHERE telegram_id = $1`,
            values,
        });

        return response.rowCount === 1;
    }

    public async setExercises(userId: number, chatId: number, exercises: Exercises): Promise<boolean> {
        const isSet = await this.setUserWeights(userId, exercises);

        if (!isSet) {
            const response = await this.databaseService.query({
                name: 'set-user-big5',
                text: `INSERT INTO mrspeaker.big5
                       (telegram_id, deadlift, bench_press, squat, shoulder_press, barbell_row)
                       VALUES ($1, 0, 0, 0, 0, 0)`,
                values: [userId],
            });

            return await this.setUserWeights(userId, exercises);
        }

        return true;
    }

    public async getUser(userId: number): Promise<UserExercise> {
        return await this.getUserDetails(userId);
    }

    public async getChannelUsers(chatId: string): Promise<ChannelExercise[]> {
        const channelExercises = await this.databaseService.query({
            name: 'get-channel-big5',
            text: `SELECT u.username,
                          COALESCE(big5.squat, 0) + COALESCE(big5.deadlift, 0) + COALESCE(big5.bench_press, 0) +
                          COALESCE(big5.barbell_row, 0) + COALESCE(big5.shoulder_press, 0) as total
                   FROM mrspeaker.big5
                            JOIN mrspeaker.chats c on big5.telegram_id = c.telegram_id
                            JOIN mrspeaker.users u on c.telegram_id = u.telegram_id
                   WHERE c.chat_id = $1`,
            values: [chatId],
        });

        if (channelExercises.rowCount === 0) {
            return null;
        }

        return channelExercises.rows;
    }
}
