import Exercises from '../../model/big5/exercises.js';

enum ExerciseType {
    SQUAT,
    DEADLIFT,
    SHOULDER_PRESS,
    BENCH_PRESS,
    BARBELL_ROW
}

export default class Big5InputService {
    public handle(input: string): Exercises {
        const exercises: Exercises = {};
        const inputParts = input.trim().split(' ');

        if (inputParts.length % 2 === 1) {
            throw new Error('Odd number of message parts, something is wrong.');
        }

        for (let i = 0; i < inputParts.length; i++) {
            const segment = inputParts[i];

            if (!isNaN(parseFloat(segment))) {
                continue;
            }

            if (segment.toLowerCase().startsWith('sq')) {
                exercises.squat = parseFloat(inputParts[i + 1]);
            }
            else if (segment.toLowerCase().startsWith('sh')) {
                exercises.shoulder_press = parseFloat(inputParts[i + 1]);
            }
            else if (segment.toLowerCase().startsWith('d')) {
                exercises.deadlift = parseFloat(inputParts[i + 1]);
            }
            else if (segment.toLowerCase().startsWith('ba') || inputParts[i].startsWith('r')) {
                exercises.barbell_row = parseFloat(inputParts[i + 1]);
            }
            else if (segment.toLowerCase().startsWith('be')) {
                exercises.bench_press = parseFloat(inputParts[i + 1]);
            }
            else {
                throw new Error(`${segment} is not a recognised exercise`);
            }
        }

        return exercises;
    }
}
