import DatabaseService from './DatabaseService.js';
import CommandResponse from '../model/CommandResponse.js';
import City from '../model/owm/City.js';
import WeatherReport from '../model/owm/WeatherReport.js';
import WeatherReportService from './WeatherReportService.js';
import OpenWeatherMapService from './OpenWeatherMapService.js';

interface Coords {
    latitude: string;
    longitude: string;
    location: string;
}

export default class WeatherService {
    private databaseService: DatabaseService;
    private openWeatherMapService: OpenWeatherMapService;
    private weatherReportService: WeatherReportService;

    constructor(databaseService: DatabaseService, openWeatherMapService: OpenWeatherMapService, weatherReportService: WeatherReportService) {
        this.databaseService = databaseService;
        this.openWeatherMapService = openWeatherMapService;
        this.weatherReportService = weatherReportService;
    }

    public async setLocation(search: string, userId: string): Promise<CommandResponse> {
        if (search === '') {
            return {
                message: 'You must specify a location.',
            }
        }

        // send location to geo api
        const city: City = await this.openWeatherMapService.geoLookup(search);
        if (city === null) {
            return {
                message: `Could not find a location matching '${search}'.`,
            }
        }

        if (await this.getLocation(userId) === null) {
            const query = {
                name: 'set-weather',
                text: 'INSERT INTO mrspeaker.weather (telegram_id, name, state, country, lat, lon) VALUES ($1, $2, $3, $4, $5, $6)',
                values: [userId, city.name, city.state, city.country, city.lat, city.lon],
            };

            await this.databaseService.query(query);
        }
        else {
            const query = {
                name: 'update-weather',
                text: 'UPDATE mrspeaker.weather SET name = $2, state = $3, country = $4, lat = $5, lon = $6 WHERE telegram_id = $1',
                values: [userId, city.name, city.state, city.country, city.lat, city.lon],
            };

            await this.databaseService.query(query);
        }

        const weatherReport: WeatherReport = await this.openWeatherMapService.getWeatherReport(city.lat, city.lon);

        const intro: string = `Set to ${city.name}, ${city.state}, ${city.country} (${city.lat},${city.lon})`;
        const report: string = this.weatherReportService.buildReport(city, weatherReport);

        return {
            message: `${intro}\n${report}`,
            options: {
                parse_mode: 'MarkdownV2',
            },
        };
    }

    private async getLocation(userId: string): Promise<City> {
        const query = {
            name: 'get-location',
            text: 'SELECT name, state, country, lat, lon FROM mrspeaker.weather WHERE telegram_id = $1',
            values: [userId],
        };

        const response = await this.databaseService.query(query);

        return response.rowCount === 0 ? null : response.rows[0];
    }

    public async getWeather(location: string | null = null, userId: string): Promise<CommandResponse> {
        let city: City;
        if (location === null) {
            city = await this.getLocation(userId);
            if (!city) {
                return { message: 'You do not have a location saved.' };
            }
        }
        else {
            city = await this.openWeatherMapService.geoLookup(location);
            if (!city) {
                return { message: `Could not find the location ${location}.` };
            }
        }

        const weatherReport: WeatherReport = await this.openWeatherMapService.getWeatherReport(city.lat, city.lon);
        return {
            message: this.weatherReportService.buildReport(city, weatherReport),
            options: {
                parse_mode: 'MarkdownV2',
            },
        };
    }
}
