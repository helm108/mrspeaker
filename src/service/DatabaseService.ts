import {Pool, QueryConfig} from 'pg';

export default class DatabaseService {
    private pool: Pool;

    constructor(pool: Pool) {
        this.pool = pool;
    }

    public async query(query: QueryConfig) {
        try {
            return await this.pool.query(query);
        } catch (e) {
            throw new Error('Database connection failure');
        }
    }
}
