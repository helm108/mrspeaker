import seedrandom from 'seedrandom';

export default class SeededRandom {
    public random(value: string): number {
        return seedrandom(value)();
    }
}
