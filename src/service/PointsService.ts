import DatabaseService from './DatabaseService.js';
import PointAssignment from '../model/PointAssignment.js';

export default class PointsService {
    private databaseService: DatabaseService;

    constructor(databaseService: DatabaseService) {
        this.databaseService = databaseService;
    }

    private async getUserPoints(userId: number, chatId: number): Promise<number> {
        const query = {
            name: 'get-points',
            text: 'SELECT points FROM mrspeaker.points WHERE telegram_id = $1 AND chat_id = $2',
            values: [userId, chatId],
        };

        const response = await this.databaseService.query(query);

        if (response.rowCount === 0) {
            const query = {
                name: 'create-points',
                text: 'INSERT INTO mrspeaker.points (telegram_id, chat_id, points) VALUES ($1, $2, $3)',
                values: [userId, chatId, 0],
            };

            await this.databaseService.query(query);
        }

        return response.rowCount === 0 ? 0 : parseInt(response.rows[0].points);
    }

    private async setPoints(userId: number, chatId: number, points: number) {
        const query = {
            name: 'set-points',
            text: 'UPDATE mrspeaker.points SET points = $3 WHERE telegram_id = $1 AND chat_id = $2',
            values: [userId, chatId, points],
        };
        await this.databaseService.query(query);
    }

    async addPoints(userId: number, chatId: number, points: number): Promise<number> {
        const currentPoints = await this.getUserPoints(userId, chatId);
        const newPoints = currentPoints + points;
        await this.setPoints(userId, chatId, newPoints);
        return newPoints;
    }

    async getPointsForChatRoom(chatId: number): Promise<PointAssignment[] | null> {
        const query = {
            name: 'list-points',
            text: `SELECT username, points
                   FROM mrspeaker.points
                            JOIN mrspeaker.users u on points.telegram_id = u.telegram_id
                   WHERE chat_id = $1`,
            values: [chatId],
        };

        const result = await this.databaseService.query(query);
        return result.rowCount === 0 ? null : result.rows;
    }
}
