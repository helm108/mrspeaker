export default class EnvVarService {
    private readonly _isProduction: boolean = null;
    private readonly _isLocal: boolean = null;
    private readonly _appName: string = null;
    private readonly _telegramBotToken: string = null;
    private readonly _telegramApiId: string = null;
    private readonly _telegramApiHash: string = null;
    private readonly _port: string = null;
    private readonly _databaseUrl: string = null;
    private readonly _ownerTelegramId: string = null;
    private readonly _openWeatherMapApiKey: string = null;

    constructor() {
        this._isLocal = process.env.NODE_ENV !== 'production';
        this._isProduction = process.env.NODE_ENV === 'production';
        this._appName = process.env.FLY_APP_NAME;
        this._telegramBotToken = process.env.TELEGRAM_BOT_TOKEN;
        this._telegramApiId = process.env.TELEGRAM_API_ID;
        this._telegramApiHash = process.env.TELEGRAM_API_HASH;
        this._port = process.env.PORT;
        this._databaseUrl = process.env.DATABASE_URL;
        this._ownerTelegramId = process.env.OWNER_TELEGRAM_ID;
        this._openWeatherMapApiKey = process.env.OPEN_WEATHER_MAP_API_KEY;
    }

    get isProduction(): boolean {
        if (this._isProduction === null) {
            throw new Error('NODE_ENV must be set.')
        }
        return this._isProduction;
    }

    get isLocal(): boolean {
        if (this._isLocal === null) {
            throw new Error('NODE_ENV must be set.')
        }
        return this._isLocal;
    }

    get appName(): string {
        if (!this._appName) {
            throw new Error('FLY_APP_NAME must be set.')
        }
        return this._appName;
    }

    get telegramBotToken(): string {
        if (!this._telegramBotToken) {
            throw new Error('TELEGRAM_BOT_TOKEN must be set.')
        }
        return this._telegramBotToken;
    }

    get telegramApiId(): string {
        if (!this._telegramApiId) {
            throw new Error('TELEGRAM_API_ID must be set.')
        }
        return this._telegramApiId;
    }

    get telegramApiHash(): string {
        if (!this._telegramApiHash) {
            throw new Error('TELEGRAM_API_HASH must be set.')
        }
        return this._telegramApiHash;
    }

    get port(): string {
        return this._port || '8080';
    }

    get databaseUrl(): string {
        if (!this._databaseUrl) {
            throw new Error('DATABASE_URL must be set.')
        }
        return this._databaseUrl;
    }

    get ownerTelegramId(): string {
        if (!this._ownerTelegramId) {
            throw new Error('OWNER_TELEGRAM_ID must be set.')
        }
        return this._ownerTelegramId;
    }

    get openWeatherMapApikey(): string {
        if (!this._openWeatherMapApiKey) {
            throw new Error('OPEN_WEATHER_MAP_API_KEY must be set.')
        }
        return this._openWeatherMapApiKey;
    }
}
