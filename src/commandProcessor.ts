import {BotCommand, Update} from 'typegram';
import {Context, Telegraf} from 'telegraf';
import pg from 'pg';

import CommandClass from './model/CommandClass';
import SeededRandom from './service/SeededRandom.js';
import Help from './command/help.js';
import Times from './command/times.js';
import YesNo from './command/yesno.js';
import Points from './command/points.js';
import Weather from './command/weather.js';
import Big5 from './command/big5.js';

import MTProtoService from './service/MTProtoService.js';
import PointsService from './service/PointsService.js';
import EnvVarService from './service/EnvVarService.js';
import UserService from './service/UserService.js';
import DatabaseService from './service/DatabaseService.js';
import OutputService from './service/OutputService.js';
import CommandHelper from './helper/command-helper.js';
import WeatherService from './service/WeatherService.js';
import WeatherReportService from './service/WeatherReportService.js';
import OpenWeatherMapService from './service/OpenWeatherMapService.js';
import Big5Service from './service/big5/Big5Service.js';
import Big5InputService from './service/big5/Big5InputService.js';
import Big5QueryService from './service/big5/Big5QueryService.js';

const envVarService = new EnvVarService();
const {Pool} = pg;

/**
 * const data = {
 *     chatId: ctx.update.message.chat.id,
 *     messageId: ctx.update.message.message_id,
 *     userId: ctx.update.message.from.id,
 *     username: ctx.update.message.from.username,
 * };
 */
export async function setCommands(bot: Telegraf<Context<Update>>, api: MTProtoService): Promise<CommandClass[]> {
    const pool = new Pool({connectionString: envVarService.databaseUrl});
    const databaseService = new DatabaseService(pool);

    const outputService = new OutputService();
    const pointsService = new PointsService(databaseService);
    const userService = new UserService(api, databaseService);
    const weatherReportService = new WeatherReportService(outputService);
    const openWeatherMapService = new OpenWeatherMapService(envVarService);
    const weatherService = new WeatherService(databaseService, openWeatherMapService, weatherReportService);
    const big5InputService = new Big5InputService();
    const big5QueryService = new Big5QueryService(databaseService);
    const big5Service = new Big5Service(big5InputService, big5QueryService, userService, outputService);

    const commandHelper = new CommandHelper();

    const commands: CommandClass[] = [
        new Help(),
        new Times(outputService),
        new YesNo(new SeededRandom()),
        new Points(userService, pointsService, outputService),
        new Weather(weatherService),
        new Big5(big5Service),
    ];

    const commandInstructions: BotCommand[] = [];

    commands.forEach(command => {
        commandInstructions.push({
            command: command.name,
            description: command.description,
        });

        bot.command(command.name, async (ctx) => {
            const userId = ctx.update.message.from.id;
            const chatId = ctx.update.message.chat.id;
            const username = ctx.update.message.from.username;

            // Update username in users table.
            try {
                await userService.addUser(userId, username);
                await userService.addUserChatAssociation(userId, chatId);
            } catch (e) {
                console.error(e);
                ctx.reply(e.message);
                return;
            }

            let message: string;
            let options: any = {};

            try {
                const cleanText = commandHelper.stripCommand(ctx.update.message.text);
                const response = await command.handle(ctx, cleanText);
                message = response.message;
                options = response.options || {};
            } catch (e) {
                console.log(e);
                message = e.message;
            }

            if (options.parse_mode === 'MarkdownV2') {
                message = outputService.escapeMarkdownV2(message);
            }

            ctx.reply(message, options);
        });
    });

    await bot.telegram.setMyCommands(commandInstructions);

    return commands;
}
