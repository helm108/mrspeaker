import {Context, Telegraf} from 'telegraf';
import {Update} from 'typegram';
import EnvVarService from './service/EnvVarService';

import express, {Application} from 'express';
import {engine} from "express-handlebars";
import CommandClass from "./model/CommandClass";

export default class Server {
    private env: EnvVarService

    constructor(envVarService: EnvVarService) {
        this.env = envVarService;
    }

    public async createServer(bot: Telegraf<Context<Update>>, secretPathComponent: string, commands: CommandClass[]): Promise<Application> {
        const app = express();
        app.engine('.hbs', engine({extname: '.hbs'}));
        app.set('view engine', '.hbs');
        app.set('views', '/app/dist/web');

        app.get('/', (req, res) => {
            res.render('index', {
                commands: commands.map(({name, description}: CommandClass) => ({name, description}))
            });
        });

        app.get('/status', (req, res) => {
            res.send(`${this.env.appName} - ${this.env.isProduction ? 'production' : 'not production'}`)
        });

        if (this.env.isProduction && secretPathComponent) {
            app.use(await bot.createWebhook({
                domain: `https://${this.env.appName}.fly.dev`,
            }));
        }

        return app;
    }
}
