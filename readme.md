# Run Locally
`docker-compose up`

# Fly
Create apps
- mr-speaker-staging
- mr-speaker-prod

Create db
- mr-speaker-db

Attach db to both apps

Monitor database since you're responsible for it not falling over

create database
```shell
flyctl postgres create
```

attach database
```shell
flyctl postgres attach mr-speaker-db --app mr-speaker-staging
flyctl postgres attach mr-speaker-db --app mr-speaker-prod
```

set secrets
```shell
fly secrets set KEY=value --app mr-speaker-staging
```

Secrets Set:
- NODE_ENV=production
- PORT=
- TELEGRAM_BOT_TOKEN=
- DATABASE_URL=postgresql://local:local@localhost:5431/mr-speaker-local
- FLY_APP_NAME=mr-speaker-local
.env file is only used locally

run locally http://localhost:8080
```shell
npm run build && npm run start
# or
docker-compose up
```

deploy
```shell
npm run deploy-staging
npm run deploy-prod
```

# Database
flyway migrations

```shell
npm run clean-local # wipe database
npm run migrate-local # provision database
```

# CI
## DB Migration
flyway needs to be able to connect to the Postgres instance in Fly from GitlabCI.
Fly doesn't allow external connections without a WireGuard connection so either `flyway` or `psql` must be used to proxy.

# Username/User ID Access Process
When a user uses a command, their username is updated in the users table.
When a user references another username, that username is used to pull their telegram ID from the users table.
If they don't exist in the users table, their ID is pulled from the Telegram API and added to the users table.
If they do exist in the users table but they were last updated too long ago, their data will be updated from the API.

# New Command
1. Create a class in `src/command` that implements `CommandClass`
2. Add that command to the `commands` array in `commandProcessor.ts`
