#!/bin/bash

# Build and Push base image to GitLab Docker Registry.
docker build -t registry.gitlab.com/helm108/mrspeaker/ci-base-image .
docker push registry.gitlab.com/helm108/mrspeaker/ci-base-image
