import DatabaseService from '../../src/service/DatabaseService';
import { Pool } from 'pg';
import PointsService from '../../src/service/PointsService';

jest.mock('pg');
jest.mock('../../src/service/DatabaseService');

describe('Points Service', () => {
    let databaseService: DatabaseService;
    let pointsService: PointsService;

    beforeEach(() => {
        databaseService = new DatabaseService(new Pool());
        pointsService = new PointsService(databaseService);
    });

    it('should set points to 0 if user does not exist in the points table', () => {
        const userId = 1;
        const chatId = 2;
        const points = 10;

        const databaseServiceGetSpy = jest.spyOn(databaseService, 'query')
            .mockImplementationOnce(query => new Promise(resolve => {
                expect(query.values).toEqual([userId, chatId]);
                resolve({
                    rows: [],
                    rowCount: 0,
                    command: 'SELECT',
                    oid: 0,
                    fields: [],
                });
            }));

        const databaseServiceInsertSpy = jest.spyOn(databaseService, 'query')
            .mockImplementationOnce(query => new Promise(resolve => {
                expect(query.values).toEqual([userId, chatId, 0])
                resolve({
                    rows: [],
                    rowCount: 0,
                    command: 'INSERT',
                    oid: 0,
                    fields: [],
                });
            }));

        const databaseServiceUpdateSpy = jest.spyOn(databaseService, 'query')
            .mockImplementationOnce(query => new Promise(resolve => {
                expect(query.values).toEqual([userId, chatId, points])
                resolve({
                    rows: [],
                    rowCount: 1,
                    command: 'UPDATE',
                    oid: 0,
                    fields: [],
                });
            }));

        pointsService.addPoints(userId, chatId, points);

        expect(databaseServiceGetSpy).toHaveBeenCalledTimes(1);
        expect(databaseServiceInsertSpy).toHaveBeenCalledTimes(1);
        expect(databaseServiceUpdateSpy).toHaveBeenCalledTimes(1);
    });

    it('should add points to a user', () => {
        const userId = 1;
        const chatId = 2;
        const points = 10;
        const currentPoints = 10;
        const newPoints = 20;

        const databaseServiceGetSpy = jest.spyOn(databaseService, 'query')
            .mockImplementationOnce(query => new Promise(resolve => {
                expect(query.values).toEqual([userId, chatId]);
                resolve({
                    rows: [{points: currentPoints}],
                    rowCount: 1,
                    command: 'SELECT',
                    oid: 0,
                    fields: [],
                });
            }));

        const databaseServiceUpdateSpy = jest.spyOn(databaseService, 'query')
            .mockImplementationOnce(query => new Promise(resolve => {
                expect(query.values).toEqual([userId, chatId, newPoints])
                resolve({
                    rows: [],
                    rowCount: 1,
                    command: 'UPDATE',
                    oid: 0,
                    fields: [],
                });
            }));

        pointsService.addPoints(userId, chatId, points);

        expect(databaseServiceGetSpy).toHaveBeenCalledTimes(1);
        expect(databaseServiceUpdateSpy).toHaveBeenCalledTimes(1);
    });

    it('should subtract points from a user', () => {
        const userId = 1;
        const chatId = 2;
        const points = -5;
        const currentPoints = 10;
        const newPoints = 5;

        const databaseServiceGetSpy = jest.spyOn(databaseService, 'query')
            .mockImplementationOnce(query => new Promise(resolve => {
                expect(query.values).toEqual([userId, chatId]);
                resolve({
                    rows: [{points: currentPoints}],
                    rowCount: 1,
                    command: 'SELECT',
                    oid: 0,
                    fields: [],
                });
            }));

        const databaseServiceUpdateSpy = jest.spyOn(databaseService, 'query')
            .mockImplementationOnce(query => new Promise(resolve => {
                expect(query.values).toEqual([userId, chatId, newPoints])
                resolve({
                    rows: [],
                    rowCount: 1,
                    command: 'UPDATE',
                    oid: 0,
                    fields: [],
                });
            }));

        pointsService.addPoints(userId, chatId, points);

        expect(databaseServiceGetSpy).toHaveBeenCalledTimes(1);
        expect(databaseServiceUpdateSpy).toHaveBeenCalledTimes(1);
    });
});
