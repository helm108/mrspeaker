import {Telegraf} from 'telegraf';
import EnvVarService from '../../src/service/EnvVarService';
import BotService from '../../src/service/BotService';

describe('Bot Service', () => {
    let telegraf: Telegraf;
    let envVarService: EnvVarService;
    let botService: BotService;

    let webhookSpy;
    let launchSpy;

    beforeEach(() => {
        process.env.FLY_APP_NAME = 'TEST_APP';
    });

    function initBotService() {
        telegraf = new Telegraf('test-token');
        envVarService = new EnvVarService();
        botService = new BotService(envVarService, telegraf);

        webhookSpy = jest.spyOn(telegraf.telegram, 'setWebhook')
            .mockImplementation(args => new Promise(resolve => resolve(true)));

        launchSpy = jest.spyOn(telegraf, 'launch')
            .mockImplementation(args => new Promise(resolve => resolve()));
    }

    it('should not use polling when NODE_ENV is "production"', async () => {
        console.log = jest.fn();

        process.env.NODE_ENV = 'production';
        initBotService();

        await botService.createBot();

        expect(webhookSpy).toHaveBeenCalledTimes(0);
        expect(launchSpy).toHaveBeenCalledTimes(0);
        expect(console.log).toHaveBeenCalledWith('==== Setting Webhook');
    });

    it('should use polling when NODE_ENV is anything other than "production"', () => {
        console.log = jest.fn();

        process.env.NODE_ENV = 'local';
        initBotService();

        botService.createBot();

        expect(launchSpy).toHaveBeenCalled();
        expect(webhookSpy).toHaveBeenCalledTimes(0);
        expect(console.log).toHaveBeenCalledWith('==== Polling');
    });
});
