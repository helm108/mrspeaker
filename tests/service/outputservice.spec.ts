import OutputService from '../../src/service/OutputService';

describe('OutputService', () => {
  let outputService: OutputService;

  beforeEach(() => {
    outputService = new OutputService();
  });

  describe('toTable', () => {
    it('returns expected values', async () => {
      const data = [
        ['short', 'other value'],
        ['so much longer', '1']
      ];

      const actual = outputService.toTable(data);

      const expected = `\`\`\`
short          other value
so much longer 1          
\`\`\``;

      expect(actual).toEqual(expected);
    });

  });
});
