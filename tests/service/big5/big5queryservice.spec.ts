import {Pool} from 'pg';

import Big5QueryService from '../../../src/service/big5/Big5QueryService';
import DatabaseService from '../../../src/service/DatabaseService';
import Exercises from '../../../src/model/big5/exercises';

jest.mock('../../../src/service/DatabaseService');

describe('Big5QueryService', () => {
    let databaseService: DatabaseService;
    let big5QueryService: Big5QueryService;

    beforeEach(() => {
        databaseService = new DatabaseService(new Pool());
        big5QueryService = new Big5QueryService(databaseService);
    });

    describe('set exercises', () => {
        const setCases: { exercises: Exercises, expectedText: string, expectedValues: (string|number)[] }[] = [
            {
                exercises: {squat: 50},
                expectedText: `UPDATE mrspeaker.big5
                   SET squat = $2
                   WHERE telegram_id = $1`,
                expectedValues: [1, 50],
            },
            {
                exercises: {squat: 50, deadlift: 55, bench_press: 60},
                expectedText: `UPDATE mrspeaker.big5
                   SET squat = $2, deadlift = $3, bench_press = $4
                   WHERE telegram_id = $1`,
                expectedValues: [1, 50, 55, 60],
            }
        ];

        it.each(setCases)('should build the expected query', async ({exercises, expectedText, expectedValues}) => {
            jest.spyOn(databaseService, 'query').mockResolvedValue({
                rows: [],
                rowCount: 1,
                oid: 0,
                fields: [],
                command: '',
            });

            const isSet: boolean = await big5QueryService.setExercises(1, 1, exercises);
            expect(databaseService.query).toHaveBeenNthCalledWith(1, {
                'name': 'set-big5',
                'text': expectedText,
                'values': expectedValues,
            });
            expect(isSet).toBe(true);
        });

        it('should return an error if no exercises passed', async () => {
            await expect(big5QueryService.setExercises(1, 1, {}))
                .rejects
                .toThrow('No exercises set.');
        });
    });
});
