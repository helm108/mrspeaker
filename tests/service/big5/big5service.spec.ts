import {Pool} from 'pg';
import MTProto from '@mtproto/core';

import Big5Service from '../../../src/service/big5/Big5Service';
import Big5InputService from '../../../src/service/big5/Big5InputService';
import Big5QueryService from '../../../src/service/big5/Big5QueryService';
import UserService from '../../../src/service/UserService';
import OutputService from '../../../src/service/OutputService';
import DatabaseService from '../../../src/service/DatabaseService';
import EnvVarService from '../../../src/service/EnvVarService';
import MTProtoService from '../../../src/service/MTProtoService';
import ChannelExercise from '../../../src/model/big5/channel-exercise.js';
import CommandResponse from '../../../src/model/CommandResponse.js';

jest.mock('@mtproto/core');

describe('Big5Service', () => {
    let envVarService: EnvVarService;
    let mtProtoService: MTProtoService;
    let databaseService: DatabaseService;
    let inputService: Big5InputService;
    let queryService: Big5QueryService;
    let userService: UserService;
    let outputService: OutputService;
    let big5Service: Big5Service;

    beforeEach(() => {
        envVarService = new EnvVarService();
        mtProtoService = new MTProtoService(envVarService, new MTProto());
        databaseService = new DatabaseService(new Pool());
        inputService = new Big5InputService();
        queryService = new Big5QueryService(databaseService);
        userService = new UserService(mtProtoService, databaseService);
        outputService = new OutputService();
        big5Service = new Big5Service(inputService, queryService, userService, outputService);
    });

    it('should return the channel details', async () => {
        jest.spyOn(queryService, 'getChannelUsers')
            .mockImplementationOnce(() => Promise.resolve([
                {username: 'slumbo', total: 100},
                {username: 'glombo', total: 200},
            ]));

        const response: CommandResponse = await big5Service.getChannelDetails('1');

        const expected: string = `\`\`\`
glombo 200kg 👑
slumbo 100kg   
\`\`\``;

        expect(queryService.getChannelUsers).toBeCalledWith('1');
        expect(response.message).toEqual(expected);
    });

    it.each([
        [null], [[]]
    ])('should return an error message if there are no users', async (emptyUsers: null|ChannelExercise[]) => {
        jest.spyOn(queryService, 'getChannelUsers')
            .mockImplementationOnce(() => Promise.resolve(emptyUsers));

        const response: CommandResponse = await big5Service.getChannelDetails('1');

        const expected: string = 'No users have set their exercises.';

        expect(queryService.getChannelUsers).toBeCalledWith('1');
        expect(response.message).toEqual(expected);
    });

    it('should return the user details', async () => {
        jest.spyOn(userService, 'getUserByUsername')
            .mockImplementationOnce(() => Promise.resolve({
                username: 'slumbo',
                telegramId: 100,
            }));

        jest.spyOn(queryService, 'getUser')
            .mockImplementationOnce(() => Promise.resolve({
                id: 1,
                telegram_id: 100,
                deadlift: 100,
                squat: 100,
                bench_press: 100,
                shoulder_press: 100,
                barbell_row: 100,
            }));

        const response: CommandResponse = await big5Service.getUserDetails('slumbo');

        const expected: string = `Weights for slumbo:
\`\`\`
Squat          100kg
Deadlift       100kg
Bench Press    100kg
Shoulder Press 100kg
Barbell Rows   100kg
Total          500kg
\`\`\``;

        expect(response.message).toEqual(expected);
    });

    it('should return an error message if the user does not exist', async () => {
        jest.spyOn(userService, 'getUserByUsername')
            .mockImplementationOnce(() => Promise.resolve(null));

        const response: CommandResponse = await big5Service.getUserDetails('slumbo');

        const expected: string = 'User does not exist.';

        expect(response.message).toEqual(expected);
    });

    it('should return an error message if the user has not set their exercises', async () => {
        jest.spyOn(userService, 'getUserByUsername')
            .mockImplementationOnce(() => Promise.resolve({
                username: 'slumbo',
                telegramId: 100,
            }));

        jest.spyOn(queryService, 'getUser')
            .mockImplementationOnce(() => Promise.resolve(null));

        const response: CommandResponse = await big5Service.getUserDetails('slumbo');

        const expected: string = 'User does not have any exercises set.';

        expect(response.message).toEqual(expected);
    });

    it('should set values for the user', async () => {
        jest.spyOn(queryService, 'getUser')
            .mockImplementationOnce(() => Promise.resolve(null));
        jest.spyOn(queryService, 'setExercises')
            .mockImplementationOnce(() => Promise.resolve(true));

        jest.spyOn(queryService, 'getUser')
            .mockImplementationOnce(() => Promise.resolve({
                id: 1,
                telegram_id: 100,
                deadlift: 100,
                squat: 100,
                bench_press: 100,
                shoulder_press: 100,
                barbell_row: 100,
            }));

        const cleanText: string = 'd 100 sq 100 bench 100 sh 100 bar 100';

        const response: CommandResponse = await big5Service.setValues(100, 1, cleanText);

        const expected: string = `Weights updated:

\`\`\`
Squat          100kg
Deadlift       100kg
Bench Press    100kg
Shoulder Press 100kg
Barbell Rows   100kg
Total          500kg
\`\`\``;

        expect(queryService.setExercises).toBeCalledWith(100, 1, {
            deadlift: 100,
            squat: 100,
            bench_press: 100,
            shoulder_press: 100,
            barbell_row: 100,
        });
        expect(response.message).toEqual(expected);
    });

    it('should allow decimal places', async () => {
        jest.spyOn(queryService, 'getUser')
            .mockImplementationOnce(() => Promise.resolve(null));
        jest.spyOn(queryService, 'setExercises')
            .mockImplementationOnce(() => Promise.resolve(true));

        jest.spyOn(queryService, 'getUser')
            .mockImplementationOnce(() => Promise.resolve({
                id: 1,
                telegram_id: 100,
                deadlift: 97.25,
                squat: 0,
                bench_press: 57.5,
                shoulder_press: 0,
                barbell_row: 0,
            }));

        const cleanText: string = 'd 97.25 be 57.5';

        const response: CommandResponse = await big5Service.setValues(100, 1, cleanText);

        const expected: string = `Weights updated:

\`\`\`
Squat          0kg     
Deadlift       97.25kg 
Bench Press    57.5kg  
Shoulder Press 0kg     
Barbell Rows   0kg     
Total          154.75kg
\`\`\``;

        expect(queryService.setExercises).toBeCalledWith(100, 1, {
            deadlift: 97.25,
            bench_press: 57.5,
        });
        expect(response.message).toEqual(expected);
    });

    it('should return an error message if setting the exercises fails', async () => {
        jest.spyOn(queryService, 'getUser')
            .mockImplementationOnce(() => Promise.resolve(null));
        jest.spyOn(queryService, 'setExercises')
            .mockImplementationOnce(() => Promise.resolve(false));

        const cleanText: string = 'd 100 sq 100 bench 100 sh 100 bar 100';

        const response: CommandResponse = await big5Service.setValues(100, 1, cleanText);

        const expected: string = 'Failed to set exercises.';

        expect(queryService.setExercises).toBeCalledWith(100, 1, {
            deadlift: 100,
            squat: 100,
            bench_press: 100,
            shoulder_press: 100,
            barbell_row: 100,
        });
        expect(response.message).toEqual(expected);
    });

    it('should show a total increase if there is one', async () => {
        jest.spyOn(queryService, 'getUser')
            .mockImplementationOnce(() => Promise.resolve({
                id: 1,
                telegram_id: 100,
                deadlift: 90,
                squat: 90,
                bench_press: 90,
                shoulder_press: 90,
                barbell_row: 90,
            }));
        jest.spyOn(queryService, 'setExercises')
            .mockImplementationOnce(() => Promise.resolve(true));

        jest.spyOn(queryService, 'getUser')
            .mockImplementationOnce(() => Promise.resolve({
                id: 1,
                telegram_id: 100,
                deadlift: 100,
                squat: 100,
                bench_press: 100,
                shoulder_press: 100,
                barbell_row: 100,
            }));

        const cleanText: string = 'd 100 sq 100 bench 100 sh 100 bar 100';

        const response: CommandResponse = await big5Service.setValues(100, 1, cleanText);

        const expected: string = `Weights updated:

\`\`\`
Squat          100kg
Deadlift       100kg
Bench Press    100kg
Shoulder Press 100kg
Barbell Rows   100kg
Total          500kg
\`\`\`
Total increased by 50kg from 450kg to 500kg`;

        expect(queryService.setExercises).toBeCalledWith(100, 1, {
            deadlift: 100,
            squat: 100,
            bench_press: 100,
            shoulder_press: 100,
            barbell_row: 100,
        });
        expect(response.message).toEqual(expected);
    });
});
