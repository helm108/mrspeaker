import Big5InputService from '../../../src/service/big5/Big5InputService';
import Exercises from '../../../src/model/big5/exercises';

describe('Big5 Input Service', () => {
    let big5InputService: Big5InputService;

    beforeEach(() => {
        big5InputService = new Big5InputService();
    });

    const successCases: {input: string, expected: Exercises}[] = [
        {
            input: 'squat 50',
            expected: {squat: 50}
        },
        {
            input: 'sq 50',
            expected: {squat: 50}
        },
        {
            input: 'deadlift 50',
            expected: {deadlift: 50}
        },
        {
            input: 'shoulder_press 50',
            expected: {shoulder_press: 50}
        },
        {
            input: 'sh 50',
            expected: {shoulder_press: 50}
        },
        {
            input: 'deadlift 50 squat 40',
            expected: {deadlift: 50, squat: 40}
        },
        {
            input: 'd 50 sq 40',
            expected: {deadlift: 50, squat: 40}
        },
        {
            input: 'barbell_row 50',
            expected: {barbell_row: 50}
        },
        {
            input: 'ba 50',
            expected: {barbell_row: 50}
        },
        {
            input: 'r 50',
            expected: {barbell_row: 50}
        },
        {
            input: 'BEN 10',
            expected: {bench_press: 10}
        },
    ];

    it.each(successCases)('should handle "%s"', ({input, expected}) => {
        const actual: Exercises = big5InputService.handle(input);
        expect(actual).toEqual(expected);
    });

    it('should throw an error if it does not recognise a segment', () => {
        expect(() => big5InputService.handle('thunder 50'))
            .toThrow('thunder is not a recognised exercise');
    });
});
