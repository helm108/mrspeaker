import WeatherService from '../../src/service/WeatherService';
import EnvVarService from '../../src/service/EnvVarService';
import DatabaseService from '../../src/service/DatabaseService';
import {Pool} from 'pg';
import WeatherReportService from '../../src/service/WeatherReportService';
import OutputService from '../../src/service/OutputService';
import Weather from '../../src/command/weather';
import createCtx from '../create-ctx';
import CommandResponse from '../../src/model/CommandResponse';
import OpenWeatherMapService from '../../src/service/OpenWeatherMapService';
import WeatherReport from '../../src/model/owm/WeatherReport';
import City from '../../src/model/owm/City';

jest.mock('../../src/service/DatabaseService');
jest.mock('../../src/service/OpenWeatherMapService');

function createWeatherReport(): WeatherReport {
    return {
        timezone: 'Australia/Melbourne',
        timezone_offset: '0',
        current: {
            dew_point: 1,
            dt: 1234567890,
            feels_like: 20,
            humidity: 21,
            pressure: 23,
            sunrise: 1234567890,
            sunset: 1234667890,
            uvi: 10,
            temp: 22,
            visibility: 24,
            wind_deg: 25,
            wind_gust: 26,
            wind_speed: 27,
            clouds: 10,
            weather: [{
                id: 1,
                main: 'Clouds',
                description: 'few clouds',
                icon: '123',
            }],
        },
        daily: [{
            clouds: 11,
            temp: {
                min: 9,
                max: 19,
                day: 9,
                eve: 9,
                morn: 19,
                night: 8,
            },
            wind_gust: 26,
            feels_like: {
                day: 9,
                eve: 9,
                morn: 19,
                night: 8,
            },
            pop: 30,
            dt: 12345767890,
            dew_point: 31,
            humidity: 32,
            uvi: 7,
            sunrise: 123456767890,
            sunset: 1235687890,
            pressure: 33,
            wind_deg: 34,
            wind_speed: 35,
            moon_phase: 3,
            moonrise: 500,
            moonset: 600,
            weather: [{
                id: 1,
                main: 'Clouds',
                description: 'few clouds',
                icon: '123',
            }],
        },
            {
                clouds: 11,
                temp: {
                    min: 9,
                    max: 19,
                    day: 9,
                    eve: 9,
                    morn: 19,
                    night: 8,
                },
                wind_gust: 26,
                feels_like: {
                    day: 9,
                    eve: 9,
                    morn: 19,
                    night: 8,
                },
                pop: 30,
                dt: 12345767890,
                dew_point: 31,
                humidity: 32,
                uvi: 7,
                sunrise: 123456767890,
                sunset: 1235687890,
                pressure: 33,
                wind_deg: 34,
                wind_speed: 35,
                moon_phase: 3,
                moonrise: 500,
                moonset: 600,
                weather: [{
                    id: 1,
                    main: 'Clouds',
                    description: 'few clouds',
                    icon: '123',
                }],
            }],
        alerts: []
    }
}

function createCity(): City {
    return {
        name: 'Name',
        state: 'State',
        country: 'Country',
        lat: '100',
        lon: '-100',
    }
}

describe('Weather', () => {
    let outputService: OutputService;
    let envVarService: EnvVarService;
    let databaseService: DatabaseService;
    let openWeatherMapService: OpenWeatherMapService;
    let weatherService: WeatherService;
    let weatherReportService: WeatherReportService;
    let weather: Weather;

    let expectedWeather: string;

    beforeEach(() => {
        envVarService = new EnvVarService();
        databaseService = new DatabaseService(new Pool());
        weatherReportService = new WeatherReportService(new OutputService());
        openWeatherMapService = new OpenWeatherMapService(envVarService);
        weatherService = new WeatherService(
            databaseService,
            openWeatherMapService,
            weatherReportService
        );

        weather = new Weather(weatherService);

        expectedWeather = `The weather for Name, State, Country:
\`\`\`
Temp        22°C (9°C to 19°C)
Feels Like  20°C              
Sunlight    10:31 to 14:18    
Weather     Few clouds        
UV Index    10 (Very High)    
Max UVI     7 (High)          
Rain Chance 3000%             
Cloud Cover 11%               
\`\`\`
Tomorrow:
\`\`\`
Temp        9°C to 19°C
Max UVI     7 (High)   
Rain Chance 3000%      
Cloud Cover 11%        
Weather     Few clouds 
\`\`\``;
    });

    describe('/weather help', () => {
        it('should return a help message', async () => {
            const actual: CommandResponse = await weather.handle(createCtx('/weather help'), 'help');
            expect(actual.message).toContain('Run \`/weather');
        });
    });

    describe('/weather', () => {
        it('should return weather if a location is saved', async () => {
            jest.spyOn(databaseService, 'query')
                .mockReturnValue(new Promise(resolve => resolve({
                    rows: [createCity()],
                    rowCount: 1,
                    command: '',
                    oid: 1,
                    fields: [],
                })));

            jest.spyOn(openWeatherMapService, 'getWeatherReport')
                .mockReturnValue(new Promise(resolve => resolve(createWeatherReport())));

            const actual: CommandResponse = await weather.handle(createCtx('/weather'), '');

            expect(databaseService.query).toHaveBeenCalled();
            expect(actual.message).toEqual(expectedWeather);
        });

        it('should return an error message if no location is saved', async () => {
            jest.spyOn(databaseService, 'query')
                .mockReturnValue(new Promise(resolve => resolve({
                    rows: [],
                    rowCount: 0,
                    command: '',
                    oid: 1,
                    fields: [],
                })));

            const actual: CommandResponse = await weather.handle(createCtx('/weather'), '');

            expect(databaseService.query).toHaveBeenCalled();
            expect(openWeatherMapService.getWeatherReport).not.toHaveBeenCalled();
            expect(actual.message).toEqual('You do not have a location saved.');
        });
    });

    describe('/weather <location>', () => {
        it('should return the weather for the given location', async () => {
            jest.spyOn(openWeatherMapService, 'geoLookup')
                .mockReturnValue(new Promise(resolve => resolve(createCity())));

            jest.spyOn(openWeatherMapService, 'getWeatherReport')
                .mockReturnValue(new Promise(resolve => resolve(createWeatherReport())));

            const actual: CommandResponse = await weather.handle(createCtx('/weather place'), 'place');

            expect(openWeatherMapService.geoLookup).toHaveBeenCalled();
            expect(openWeatherMapService.getWeatherReport).toHaveBeenCalled();
            expect(actual.message).toEqual(expectedWeather);
        });

        it('should return an error message if the given location can not be found', async () => {
            jest.spyOn(openWeatherMapService, 'geoLookup')
                .mockReturnValue(new Promise(resolve => resolve(null)));

            jest.spyOn(openWeatherMapService, 'getWeatherReport')
                .mockReturnValue(new Promise(resolve => resolve(createWeatherReport())));

            const expected: string = 'Could not find the location place.';

            const actual: CommandResponse = await weather.handle(createCtx('/weather place'), 'place');

            expect(openWeatherMapService.geoLookup).toHaveBeenCalled();
            expect(openWeatherMapService.getWeatherReport).not.toHaveBeenCalled();
            expect(actual.message).toEqual(expected);
        });
    });

    describe('/weather set', () => {
        it('should set location and return the weather if the given location can be found', async () => {
            jest.spyOn(databaseService, 'query')
                .mockReturnValue(new Promise(resolve => resolve({
                    rows: [],
                    rowCount: 0,
                    command: '',
                    oid: 1,
                    fields: [],
                })));

            jest.spyOn(openWeatherMapService, 'geoLookup')
                .mockReturnValue(new Promise(resolve => resolve(createCity())));

            jest.spyOn(openWeatherMapService, 'getWeatherReport')
                .mockReturnValue(new Promise(resolve => resolve(createWeatherReport())));

            const actual: CommandResponse = await weather.handle(createCtx('/weather set place'), 'set place');

            const expected = `Set to Name, State, Country (100,-100)\n${expectedWeather}`

            expect(databaseService.query).toHaveBeenNthCalledWith(1, {
                'name': 'get-location',
                'text': 'SELECT name, state, country, lat, lon FROM mrspeaker.weather WHERE telegram_id = $1',
                'values': [100]
            });
            expect(databaseService.query).toHaveBeenNthCalledWith(2, {
                'name': 'set-weather',
                'text': 'INSERT INTO mrspeaker.weather (telegram_id, name, state, country, lat, lon) VALUES ($1, $2, $3, $4, $5, $6)',
                'values': [100, 'Name', 'State', 'Country', '100', '-100']
            });
            expect(openWeatherMapService.geoLookup).toHaveBeenCalled();
            expect(openWeatherMapService.getWeatherReport).toHaveBeenCalled();
            expect(actual.message).toEqual(expected);
        });

        it('should overwrite location and return the weather if the given location can be found', async () => {
            jest.spyOn(databaseService, 'query')
                .mockReturnValue(new Promise(resolve => resolve({
                    rows: [createCity()],
                    rowCount: 1,
                    command: '',
                    oid: 1,
                    fields: [],
                })));

            jest.spyOn(openWeatherMapService, 'geoLookup')
                .mockReturnValue(new Promise(resolve => resolve(createCity())));

            jest.spyOn(openWeatherMapService, 'getWeatherReport')
                .mockReturnValue(new Promise(resolve => resolve(createWeatherReport())));

            const actual: CommandResponse = await weather.handle(createCtx('/weather set place'), 'set place');

            const expected = `Set to Name, State, Country (100,-100)\n${expectedWeather}`

            expect(databaseService.query).toHaveBeenNthCalledWith(1, {
                'name': 'get-location',
                'text': 'SELECT name, state, country, lat, lon FROM mrspeaker.weather WHERE telegram_id = $1',
                'values': [100]
            });
            expect(databaseService.query).toHaveBeenNthCalledWith(2, {
                'name': 'update-weather',
                'text': 'UPDATE mrspeaker.weather SET name = $2, state = $3, country = $4, lat = $5, lon = $6 WHERE telegram_id = $1',
                'values': [100, 'Name', 'State', 'Country', '100', '-100']
            });
            expect(openWeatherMapService.geoLookup).toHaveBeenCalled();
            expect(openWeatherMapService.getWeatherReport).toHaveBeenCalled();
            expect(actual.message).toEqual(expected);
        });

        it('should return an error message if no location is specified', async () => {
            const expected = 'You must specify a location.';

            const actual: CommandResponse = await weather.handle(createCtx('/weather set'), 'set');

            expect(actual.message).toEqual(expected);
        });

        it('should return an error if the given location can be found but saving it fails', async () => {
            jest.spyOn(databaseService, 'query')
                .mockRejectedValue(new Error('Database connection failure'));

            const expected = 'Database connection failure. Unable to set location.';

            const actual: CommandResponse = await weather.handle(createCtx('/weather set place'), 'set place');

            expect(actual.message).toEqual(expected);
        });

        it('should return an error message if the given location cannot be found', async () => {
            const actual: CommandResponse = await weather.handle(createCtx('/weather place'), 'place');

            expect(actual.message).toEqual('Could not find the location place.');
        });
    })
});
