import YesNo from '../../src/command/yesno';
import { Context } from 'telegraf';
import { MountMap } from 'telegraf/typings/telegram-types';
import { mock } from 'jest-mock-extended';
import CommandHelper from '../../src/helper/command-helper';

class MockSeededRandom {
    public random(value: string): number {
        return parseFloat(value);
    }
}

describe('YesNo', () => {
    let yesNo: YesNo;
    let commandHelper: CommandHelper;

    beforeEach(() => {
        yesNo = new YesNo(new MockSeededRandom());
        commandHelper = new CommandHelper();
    });

    it('should reject strings that do not end with ?', async () => {
        const ctx: Context<MountMap['text']> = mock<Context<MountMap['text']>>();
        ctx.message.text = '/mrspeaker Garbo garbo.';

        const response = await yesNo.handle(ctx, commandHelper.stripCommand(ctx.message.text));
        const actual = response.message;
        const expected = 'That is not a question.';

        expect(actual).toEqual(expected);
    });

    it.each([
        ['Yes.', 0.0],
        ['Yes.', 0.449],
        ['No.', 0.45],
        ['No.', 0.899],
        ['Maybe.', 0.9],
        ['Maybe.', 1.0],
    ])('should reply %p for the value %p', async (expected, value) => {
        const ctx: Context<MountMap['text']> = mock<Context<MountMap['text']>>();
        ctx.message.text = `/mrspeaker ${value}?`;

        const response = await yesNo.handle(ctx, commandHelper.stripCommand(ctx.message.text));
        const actual = response.message;

        expect(actual).toEqual(expected);
    });
});
