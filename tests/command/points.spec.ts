import { Pool } from 'pg';
import MTProto from '@mtproto/core';
import Points from '../../src/command/points';
import MTProtoService from '../../src/service/MTProtoService';
import DatabaseService from '../../src/service/DatabaseService';
import UserService from '../../src/service/UserService';
import OutputService from '../../src/service/OutputService';
import PointsService from '../../src/service/PointsService';
import User from '../../src/model/User';
import EnvVarService from '../../src/service/EnvVarService';
import CommandHelper from "../../src/helper/command-helper";
import createCtx from '../create-ctx';

jest.mock('@mtproto/core');
jest.mock('../../src/service/MTProtoService');
jest.mock('../../src/service/DatabaseService');

describe('Points', () => {
    let envVarService: EnvVarService;
    let mtProtoService: MTProtoService;
    let userService: UserService;
    let databaseService: DatabaseService;
    let outputService: OutputService;
    let pointsService: PointsService;
    let points: Points;
    let commandHelper: CommandHelper;

    beforeEach(() => {
        envVarService = new EnvVarService();
        mtProtoService = new MTProtoService(envVarService, new MTProto());
        databaseService = new DatabaseService(new Pool());
        userService = new UserService(mtProtoService, databaseService);
        pointsService = new PointsService(databaseService);
        outputService = new OutputService();
        points = new Points(userService, pointsService, outputService);
        commandHelper = new CommandHelper();

        const mockUser: User = {telegramId: 1, username: 'slumbo'};

        jest.spyOn(mtProtoService, 'getUser')
            .mockReturnValue(new Promise(resolve => resolve(mockUser)));

        jest.spyOn(pointsService, 'addPoints')
            .mockReturnValue(new Promise(resolve => resolve(10)));
    });

    it('should not allow a user to give themselves points', () => {
        const ctx = createCtx('/points 10 @slumbo', 'slumbo');

        expect(async () => {
            await points.handle(ctx, commandHelper.stripCommand(ctx.update.message.text));
        }).rejects.toThrow('You cannot award yourself points');
    });

    it('should allow a user to take points from themselves', () => {
        const ctx = createCtx('/points -10 @slumbo', 'slumbo');

        expect(async () => {
            await points.handle(ctx, commandHelper.stripCommand(ctx.update.message.text));
        }).rejects.toThrow('You cannot award yourself points');
    });

    it.each([
        ['/points 10 @slumbo', 'Added 10 points to @slumbo. New total: 10'],
        ['/points@bot_name 10 @slumbo', 'Added 10 points to @slumbo. New total: 10'],
        ['/points @slumbo 10', 'Added 10 points to @slumbo. New total: 10'],
        ['/points -10 @slumbo', 'Subtracted 10 points from @slumbo. New total: 10'],
        ['/points @slumbo -10', 'Subtracted 10 points from @slumbo. New total: 10'],
        ['/points 10.5 @slumbo', 'Added 10 points to @slumbo. New total: 10'],
    ])('should correctly process the command %p', async (text, expected) => {
        jest.spyOn(databaseService, 'query')
            .mockReturnValue(new Promise(resolve => resolve({
                rowCount: 1,
                rows: [{telegramId: 1, username: 'slumbo'}],
                command: '',
                oid: 1,
                fields: [],
            })));

        const ctx = createCtx(text);

        const response = await points.handle(ctx, commandHelper.stripCommand(ctx.update.message.text));
        const actual = response.message;

        expect(actual).toEqual(expected);
    });

    it.each([
        ['/points 10 slumbo jumbo', 'Too many parts to this message'],
        ['/points 10 @slum bo', 'Too many parts to this message'],
        ['/points 10 slumbo', 'You must specify a username'],
        ['/points ten slumbo', 'You must specify a username'],
        ['/points ten @slumbo', '\'ten\' is not a number.'],
    ])('should return the correct error message when given malformed command %p', async (text, expected) => {
        const ctx = createCtx(text);

        await expect(async () => {
            await points.handle(ctx, commandHelper.stripCommand(ctx.update.message.text));
        }).rejects.toThrow(expected);
    });

    it('should order points correctly', async () => {
        jest.spyOn(pointsService, 'getPointsForChatRoom')
            .mockReturnValue(new Promise(resolve => resolve([
                {username: 'martensoderberg', points: 10},
                {username: 'GregSmith', points: 5},
                {username: 'RMcM7', points: -5},
            ])));

        const ctx = createCtx('/points');

        const expected = `\`\`\`
martensoderberg 10
GregSmith       5 
RMcM7           -5
\`\`\``;

        const response = await points.handle(ctx, commandHelper.stripCommand(ctx.update.message.text));
        const actual = response.message;

        expect(actual).toEqual(expected);
    });

    it('should return a message if nobody has any points', async () => {
        jest.spyOn(pointsService, 'getPointsForChatRoom')
            .mockReturnValue(new Promise(resolve => resolve(null)));

        const ctx = createCtx('/points');

        const expected = 'Nobody has any points in this chat.';

        const response = await points.handle(ctx, commandHelper.stripCommand(ctx.update.message.text));
        const actual = response.message;
        expect(actual).toEqual(expected);
    });
});
