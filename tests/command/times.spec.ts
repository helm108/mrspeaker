import Times from '../../src/command/times';
import { Context } from 'telegraf';
import { MountMap } from 'telegraf/typings/telegram-types';
import { mock } from 'jest-mock-extended';
import OutputService from '../../src/service/OutputService';

describe('Times', () => {
    it('returns expected values', async () => {
        const times = new Times(new OutputService());

        const ctx: Context<MountMap['text']> = mock<Context<MountMap['text']>>();
        ctx.message.date = 1647539568;

        const response = await times.handle(ctx);
        const actual = response.message;

        const expected = `\`\`\`
New York  Thu 17, 13:52
London    Thu 17, 17:52
UTC       Thu 17, 17:52
Amsterdam Thu 17, 18:52
Tokyo     Fri 18, 02:52
Melbourne Fri 18, 04:52
\`\`\``;

        expect(actual).toEqual(expected);
    });
});
