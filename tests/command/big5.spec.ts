import {Pool} from 'pg';
import MTProto from '@mtproto/core';

import Big5 from '../../src/command/big5';
import Big5Service from '../../src/service/big5/Big5Service';
import Big5InputService from '../../src/service/big5/Big5InputService';
import Big5QueryService from '../../src/service/big5/Big5QueryService';
import UserService from '../../src/service/UserService';
import OutputService from '../../src/service/OutputService';
import DatabaseService from '../../src/service/DatabaseService';
import MTProtoService from '../../src/service/MTProtoService';
import EnvVarService from '../../src/service/EnvVarService';
import createCtx from '../create-ctx';

jest.mock('@mtproto/core');
jest.mock('../../src/service/big5/Big5Service');

describe('big5', () => {
    let envVarService: EnvVarService;
    let mtProtoService: MTProtoService;
    let databaseService: DatabaseService;
    let inputService: Big5InputService;
    let queryService: Big5QueryService;
    let userService: UserService;
    let outputService: OutputService;
    let big5Service: Big5Service;
    let big5: Big5;

    beforeEach(() => {
        envVarService = new EnvVarService();
        mtProtoService = new MTProtoService(envVarService, new MTProto());
        databaseService = new DatabaseService(new Pool());
        inputService = new Big5InputService();
        queryService = new Big5QueryService(databaseService);
        userService = new UserService(mtProtoService, databaseService);
        outputService = new OutputService();
        big5Service = new Big5Service(inputService, queryService, userService, outputService)
        big5 = new Big5(big5Service);
    });

    it('should return the channel details', async () => {
        jest.spyOn(big5Service, 'getChannelDetails')
            .mockImplementationOnce(() => Promise.resolve({
                message: 'Channel details',
                options: {
                    parse_mode: 'MarkdownV2',
                }
            }));

        const response = await big5.handle(createCtx('/big5', 'slumbo'), '');
        expect(big5Service.getChannelDetails).toBeCalledWith(1);
        expect(response.message).toEqual('Channel details');
    });

    it('should return the user details', async () => {
        jest.spyOn(big5Service, 'getUserDetails')
            .mockImplementationOnce(() => Promise.resolve({
                message: 'User details',
                options: {
                    parse_mode: 'MarkdownV2',
                }
            }));

        const response = await big5.handle(createCtx('/big5 @slumbo', 'slumbo'), '@slumbo');
        expect(big5Service.getUserDetails).toBeCalledWith('slumbo');
        expect(response.message).toEqual('User details');
    });

    it('should set exercise values for the given user', async () => {
        jest.spyOn(big5Service, 'setValues')
            .mockImplementationOnce(() => Promise.resolve({
                message: 'User details',
                options: {
                    parse_mode: 'MarkdownV2',
                }
            }));

        const response = await big5.handle(
            createCtx('/big5 sh 10 sq 15 d 20', 'slumbo'),
            'sh 10 sq 15 d 20');
        expect(big5Service.setValues).toBeCalledWith(100, 1, 'sh 10 sq 15 d 20');
        expect(response.message).toEqual('User details');
    });
});
