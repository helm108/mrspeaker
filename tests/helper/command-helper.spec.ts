import CommandHelper from '../../src/helper/command-helper';

describe('Command Helper', () => {
   let commandHelper: CommandHelper;

    beforeEach(() => {
        commandHelper = new CommandHelper();
    });

    it('should remove a slash command', () => {
        const expected = 'add 5 @username';
        const actual = commandHelper.stripCommand('/points add 5 @username');

        expect(actual).toEqual(expected);
    });

    it('should remove a slash command with bot name', () => {
        const expected = 'add 5 @username';
        const actual = commandHelper.stripCommand('/points@mr_speaker_local add 5 @username');

        expect(actual).toEqual(expected);
    });

    it('should return an empty string if only the command is sent', () => {
        const expected = '';
        const actual = commandHelper.stripCommand('/points');

        expect(actual).toEqual(expected);
    });
});
