export default function createCtx(text: string, fromUsername: string = 'gumbo') {
    return {
        update: {
            message: {
                text,
                chat: {
                    id: 1,
                },
                from: {
                    id: 100,
                    username: fromUsername,
                }
            }
        }
    };
}
