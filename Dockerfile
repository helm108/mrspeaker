FROM node:18-alpine AS build
WORKDIR /app
COPY . .
RUN npm ci
RUN npm run build && npm run copy

FROM node:18-alpine AS server
WORKDIR /app
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/dist ./dist
COPY --from=build /app/package.json .

EXPOSE 8080

CMD ["npm", "run", "start"]
